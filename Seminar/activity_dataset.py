#Radni zadatak je napraviti vlastiti dataset sa u kojem se nalaze podaci
#o aktivnostima (temperatura, svjetlina, količina CO2 i vlaga) u prostoriji u 
#kojoj boravim. Odradit eksplorativnu analizu, vizualizaciju podataka te napravit
#predikciju temperature prostorije.

import pandas as pd
