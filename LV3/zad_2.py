import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

automobili = pd.read_csv('mtcars.csv')

#1.   Barplot potrosnja automobila s 4, 6 i 8 cilindara
print("--------------------1-----------------------\n")
cetiri = automobili.query('cyl == 4').mpg
sest = automobili.query('cyl == 6').mpg
osam = automobili.query('cyl == 8').mpg

plt.figure(1)
plt.grid(True)
cetiri.plot(kind='bar', label="Cetiri cilindra")
sest.plot(kind='bar',color='red', label="Sest cilindara")
osam.plot(kind='bar',color='green', label="Osam cilindara")
plt.title('Potrosnja po cilindrima')
plt.ylabel('mpg')
plt.xlabel('Podjela po indeksima')
plt.legend()
plt.show()

#2.   boxplot distribucija tezine automobila s 4,6 i 8 cilindara
print("--------------------2-----------------------\n")

cetiriwt = automobili.query('cyl == 4').wt
sestwt = automobili.query('cyl == 6').wt
osamwt = automobili.query('cyl == 8').wt

plt.figure(2)
plt.grid(True)
plt.subplot(221)
plt.title('Cetiri cilindra')
cetiriwt.plot(kind='box')
plt.subplot(222)
plt.title('Sest cilindara')
sestwt.plot(kind='box',color='red')
plt.subplot(223)
plt.title('Osam cilindara')
osamwt.plot(kind='box',color='green')
plt.show()


#3.    potrosnja automatika i manuala
print("--------------------3-----------------------\n")

manual = automobili.query('am==False').mpg
automatic = automobili.query('am==True').mpg

manual_potrosnja=manual.sum()
automatic_potrosnja=automatic.sum()

print ("Manual potrosnja: {0} mpg\nAutomatic: {1} mpg".format(manual_potrosnja,automatic_potrosnja))
plt.figure(3)
plt.title('Usporedba potrosnje automatika i manuala')
plt.grid()
plt.bar(0,manual_potrosnja)
plt.bar(1,automatic_potrosnja,color='red')
plt.show()

#4.    odnos ubrzanja i snage automatika i manuala (hp,qsec)
print("--------------------4-----------------------\n")

snagamn = automobili.query("am == 0").hp
ubrzanjemn = automobili.query("am== 0").qsec
snagaat = automobili.query("am == 1").hp
ubrzanjeat = automobili.query("am== 1").qsec


